import os
from keras import metrics
import simplejson as json

import shared.predict
import shared.custom_metrics as custom_metrics
from shared.plotting import *
import shared.utility
from personal_config import TRAIN_DIR

# Model name you want to run and the epochs
model_name = r'pretrained.model2_xception'
epochs = 200
do_image_preprocess = False
amount_of_Pictures = 400
generate_pictures = False

################
# RUNNER MAGIC #
################

# Model ID and Path
model_folder = shared.utility.create_run_folder(model_name)

# For Prediction we need the correct img size (it assumes 150x150 by default)
if model_name.split('.')[0] == 'pretrained':
    from models.pretrained.pretrained_conf import *
elif model_name.split('.')[0] == 'baseline':
    from models.baseline.baseline_conf import *

# Import Model
name = 'models.' + model_name + '.model'
mod = __import__(name=name, fromlist=[''])


# Setup
model_path = os.path.join(model_folder, 'model.h5')
debug_Path = os.path.join(model_folder, 'Graph')
class_labels_path = os.path.join(model_folder, 'class_labels.txt')
history_file_path = os.path.join(model_folder, 'history.json')
training_data = os.path.join(model_folder, 'training_files.txt')
validation_data = os.path.join(model_folder, 'validation_files.txt')
preprocessing_data = os.path.join(model_folder, 'preprocessing_files.txt')
confusion_matrix_path = os.path.join(model_folder, 'confusion_matrix.png')

if "_special" in model_name:
    evaluation_metrics = [metrics.binary_accuracy,
                          custom_metrics.fmeasure,
                          custom_metrics.precision,
                          custom_metrics.recall]
else:
    evaluation_metrics = [metrics.categorical_accuracy,
                          custom_metrics.fmeasure,
                          custom_metrics.precision,
                          custom_metrics.recall]


#IMG PreProcessing Data
if do_image_preprocess:
    data_timer = shared.utility.Timer('PreProcessing Data')
    train_dir, test_dir = mod.preProcessing_img()
    data_timer.finish()
    print(data_timer)

# Prepare Data
data_timer = shared.utility.Timer('Prepare Data')
if do_image_preprocess:
    assert train_dir
    mod.prepare_data(source_directory=train_dir,
                     save_training_data=training_data,
                     save_validation_data=validation_data)
else:
    mod.prepare_data(save_training_data=training_data,
                     save_validation_data=validation_data)
data_timer.finish()
print(data_timer)

if generate_pictures:
    data_timer = shared.utility.Timer('Genberating Pictures')
    mod.generate_pictures(numberof = amount_of_Pictures)
    data_timer.finish()
    print(data_timer)


# Train Model
train_timer = shared.utility.Timer('Execute Model')
history = mod.train_model(model_save_path=model_path,
                          class_labels_path=class_labels_path,
                          evaluation_metrics=evaluation_metrics,
                          debug_path=debug_Path,
                          epochs=epochs,
                          early_stop=True,
                          best_model=True)
train_timer.finish()
print("for debugging: >> tensorboard --logdir " + debug_Path)
print(train_timer)

# Plots
for metric in evaluation_metrics:
    fig_path = os.path.join(model_folder, '{0}.png').format(metric.__name__)
    plot_training_validation_metric(history=history,
                                    metric=metric.__name__,
                                    fig_path=fig_path)

# Prediction
# ToDo: Fix this
if do_image_preprocess:
    result = shared.predict.predict(model_path,
                                    shared.predict.test_data_array(test_folder=os.path.join(test_dir, 'test')),
                                    evaluation_metrics,
                                    img_height=IMG_HEIGHT,
                                    img_width=IMG_WIDTH)
else:
    result = shared.predict.predict(model_path,
                                    shared.predict.test_data_array(),
                                    evaluation_metrics,
                                    img_height=IMG_HEIGHT,
                                    img_width=IMG_WIDTH)

shared.utility.generate_csv(folder=model_folder,
                            result=result,
                            label_dict=shared.predict.read_class_label(class_labels_path))

# Dumping history to json
with open(history_file_path, 'w') as fp:
    # lr to decimal
    if 'lr' in history.history:
        lr_object = [json.Decimal(item.item()) for item in history.history['lr']]
        history.history['lr'] = lr_object
    json.dump(history.history, fp, use_decimal=True)


# Confusion Matrix
validation_array = []
class_array = []
label_array = []
class_dict = read_class_label(label_txt_path=class_labels_path, reverse=True)

# Some Arranging of data for confusion matrix
with open(validation_data, 'r') as fi:
    for item in fi.readlines():
        couple = [item.strip() for item in item.split(r'/')]
        if do_image_preprocess:
            validation_array.append(os.path.join(train_dir+'\{0}'.format(couple[0]), couple[1]))
        else:
            validation_array.append(os.path.join(TRAIN_DIR + '\{0}'.format(couple[0]), couple[1]))
        class_array.append(class_dict[couple[0]])
        if couple[0] not in label_array:
            label_array.append(couple[0])

# Predict the Validation Files
predictions = predict.predict(h5_path=model_path,
                              picture_array=validation_array,
                              custom_metrics=evaluation_metrics,
                              img_width=IMG_WIDTH,
                              img_height=IMG_HEIGHT)
predictions = [item[1].tolist()[0] for item in predictions]
confusionMTX = sklearn.metrics.confusion_matrix(class_array, predictions)

plot_confusion_matrix(confusion_matrix=confusionMTX,
                      class_array=label_array,
                      fig_path=confusion_matrix_path)

print("Done. Saved in Folder {0}".format(model_folder))
