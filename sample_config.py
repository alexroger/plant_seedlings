import os
# Rename this file or copy content to file "personal_config.py"
DATA = r'C:\Data\plant_seedlings'
TRAIN_DIR = os.path.join(DATA, 'train')
TEST_DIR = os.path.join(DATA, 'test')

CATEGORIES = ['Black-grass', 'Charlock', 'Cleavers', 'Common Chickweed', 'Common wheat', 'Fat Hen',
              'Loose Silky-bent', 'Maize', 'Scentless Mayweed', 'Shepherds Purse',
              'Small-flowered Cranesbill', 'Sugar beet']

NUM_CATEGORIES = len(CATEGORIES)
SEED = 1234
