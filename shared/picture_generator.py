import os
import random
import shutil
import numpy as np
import pandas as pd
from personal_config import CATEGORIES, DATA, SEED
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img


#create and delete pictures
def generatePicture (
    train_dir,
    numberof=400,
    rotation_range=360,                          # degree for rotation
    width_shift_range=0.3,                      # fraction of total width/height "within which to randomly translate pictures vertically or horizontally
    height_shift_range=0.3,
    #shear_range=0.1,                            # randomly applying shearing transformations, see https://en.wikipedia.org/wiki/Shear_mapping
    zoom_range=0.3,                             # randomly zooming inside pictures
    horizontal_flip=True,                       # randomly flipping half of the images horizontally
    vertical_flip=True,
    fill_mode='nearest'):                       # filling newly created pixels strategy (because of width height shift necessary)

    # delete old generated pictures
    print('delete old generated pictures')
    for category_id, category in enumerate(CATEGORIES):
        fileList = [f for f in os.listdir(os.path.join(train_dir, category)) if f.startswith("copy")]
        for f in fileList:
            os.remove(os.path.join(train_dir, category, f))


    print('generate images that each category counts minimum ' + str(numberof))

    datagen = ImageDataGenerator(
        rotation_range = rotation_range,
        width_shift_range= width_shift_range,
        height_shift_range= height_shift_range,
        #shear_range  = shear_range,
        zoom_range = zoom_range,
        horizontal_flip= horizontal_flip,
        vertical_flip=vertical_flip,
        fill_mode= fill_mode
    )

    for category_id, category in enumerate(CATEGORIES):
            numberOfFile = len(next(os.walk(os.path.join(train_dir, category)))[2])
            print('generate ' + str(numberof - numberOfFile) + ' pictures for category: ' + category)

            while(numberof > numberOfFile):
                DIR_TO_SAVE = os.path.join(train_dir, category)

                #TODO: think about: could be better selected?
                randomPicture = random.choice(os.listdir(os.path.join(train_dir, category)))

                #print(os.path.join(TRAIN_DIR, category ,randomPicture))
                img = load_img(os.path.join(train_dir, category ,randomPicture))
                x = img_to_array(img)
                x = x.reshape((1,) + x.shape)


                i = 0
                for batch in datagen.flow(x, batch_size = 1 , save_to_dir= DIR_TO_SAVE, save_prefix='copy' + str(numberOfFile)): #to avoid overwriting
                    i += 1
                    if i >= 1:
                        break

                numberOfFile = numberOfFile + 1


if __name__ == '__main__':
    generatePicture(numberof=0) # for reset




