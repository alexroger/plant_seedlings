import cv2
import numpy as np
import os
import matplotlib.pyplot as plt
import shutil
import time
import pandas as pd
from tqdm import tqdm
from PIL import Image
from personal_config import TRAIN_DIR, DATA

# adapted from https://www.kaggle.com/gaborvecsei/plant-seedlings-fun-with-computer-vision
# adapted from https://www.kaggle.com/ianchute/background-removal-cieluv-color-thresholding


def prepImage(preprocessing_data, imgDict):
    # img dict: folder\file = (img_raw)

    print("Checking Folder Structure for preprocessing ...")
    if not os.path.isdir(preprocessing_data):
        os.mkdir(preprocessing_data)

    for key in imgDict.keys():
        save_dir = os.path.join(preprocessing_data, os.path.dirname(key))
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)
        if os.path.isfile(os.path.join(preprocessing_data, key)):
            print("Image " + key + " exists.")
            continue
        print("segment images of " + key)
        #print(imgDict)
        #image = imgDict[key]
        #image_segmented = segment_plant(image)
        #image_sharpen = sharpen_image(image_segmented)
        #plt.imshow(image_sharpen)
        #plt.axis('off')
        if preprocessing_data[-4:] == 'test':
            image_processed = process_image(os.path.join(DATA, key), is_path=True)
        else:
            image_processed = process_image(os.path.join(TRAIN_DIR, key), is_path=True)
        im = Image.fromarray(image_processed)
        im.save(os.path.join(preprocessing_data, key))
        #plt.imshow(image_processed)
        #plt.axis('off')
        #plt.savefig(os.path.join(preprocessing_data, key), bbox_inches='tight', pad_inches=0, transparent=True)


def create_mask_for_plant(image):
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    sensitivity = 35
    lower_hsv = np.array([60 - sensitivity, 100, 50])
    upper_hsv = np.array([60 + sensitivity, 255, 255])

    mask = cv2.inRange(image_hsv, lower_hsv, upper_hsv)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    return mask


def segment_plant(image):
    mask = create_mask_for_plant(image)
    output = cv2.bitwise_and(image, image, mask=mask)
    return output


def sharpen_image(image):
    image_blurred = cv2.GaussianBlur(image, (0, 0), 3)
    image_sharp = cv2.addWeighted(image, 1.5, image_blurred, -0.5, 0)
    return image_sharp


def cieluv(img, target):
    # adapted from https://www.compuphase.com/cmetric.htm
    img = img.astype('int')
    aR, aG, aB = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    bR, bG, bB = target
    rmean = ((aR + bR) / 2.).astype('int')
    r2 = np.square(aR - bR)
    g2 = np.square(aG - bG)
    b2 = np.square(aB - bB)

    # final sqrt removed for speed; please square your thresholds accordingly
    result = (((512 + rmean) * r2) >> 8) + 4 * g2 + (((767 - rmean) * b2) >> 8)

    return result


def process_image(f,
                  is_path=False,
                  black_white=False,
                  plot=False):
    #print(f)
    if is_path:
        img = plt.imread(f)
        img = np.round(img * 255).astype('ubyte')[:, :, :3]
    else:
        img = f
        #img = np.round(f).astype('ubyte')[:, :, :3]

    if plot:
        plt.figure(1)
        plt.subplot(141)
        plt.imshow(img)
        plt.title('Raw Image')
    img_filter = (
        (cieluv(img, (71, 86, 38)) > 1600)
        & (cieluv(img, (65, 79, 19)) > 1600)
        & (cieluv(img, (95, 106, 56)) > 1600)
        & (cieluv(img, (56, 63, 43)) > 500)
    )
    img[img_filter] = 0

    if plot:
        plt.subplot(142)
        plt.imshow(img)
        plt.title('CIELUV Color Thresholding')

    img = cv2.medianBlur(img, 9)

    if plot:
        plt.subplot(143)
        plt.imshow(img)
        plt.title('Median filter')

    if black_white:
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        img = img.astype('uint8')

        if plot:
            plt.subplot(144)
            plt.imshow(img)
            plt.title('Black and White')
    return img


# http://robotics.usc.edu/~ampereir/wordpress/?p=626
def save_figure_as_image(fileName, fig=None, **kwargs):
    """ Save a Matplotlib figure as an image without borders or frames.
       Args:
            fileName (str): String that ends in .png etc.

            fig (Matplotlib figure instance): figure you want to save as the image
        Keyword Args:
            orig_size (tuple): width, height of the original image used to maintain
            aspect ratio.
    """
    fig_size = fig.get_size_inches()
    w, h = fig_size[0], fig_size[1]
    fig.patch.set_alpha(0)
    if 'orig_size' in kwargs:  # Aspect ratio scaling if required
        w, h = kwargs['orig_size']
        w2, h2 = fig_size[0], fig_size[1]
        fig.set_size_inches([(w2/w)*w, (w2/w)*h])
        fig.set_dpi((w2/w)*fig.get_dpi())
    a = fig.gca()
    a.set_frame_on(False)
    a.set_xticks([])
    a.set_yticks([])
    plt.axis('off')
    plt.xlim(0, h)
    plt.ylim(w, 0)
    fig.savefig(fileName, transparent=True, bbox_inches='tight', pad_inches=0)


if __name__ == '__main__':
    plt.rcParams['image.cmap'] = 'gray'
    plt.rcParams['figure.figsize'] = (16, 9)
    plt.style.use('dark_background')
    plt.interactive(False)
    img = process_image(r'C:\Data\plant_seedlings\train\Loose Silky-bent\0db2f82ee.png', is_path=True, plot=True)
    plt.imshow(img)
    plt.show()
