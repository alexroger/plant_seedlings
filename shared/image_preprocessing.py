import numpy as np
from glob import glob
import cv2
from keras.preprocessing.image import img_to_array, load_img
from keras.utils import to_categorical
import os


def vectorize_images_in_folder(source_dir, target_width, target_height, class_label_dict):
    print("Creating Image and Label Array for {0}".format(source_dir))
    image_array = []
    label_array = []
    if len(os.listdir(source_dir)) == 0:
        raise Exception('No category subfolders found.')
    for root, dirs, files in os.walk(source_dir):
        for file_ in files:
            file_path = os.path.join(root, file_)
            cat_label = os.path.basename(root)
            img = load_img(file_path, grayscale=False, target_size=(target_height, target_width))
            x = img_to_array(img)
            # divide by 255
            x = x/ 255.
            image_array.append(x)
            label_array.append(class_label_dict[cat_label])
    print("Image and Label Array created!")
    return np.array(image_array), to_categorical(np.array(label_array), num_classes=len(class_label_dict))


def LoadImagestoArray(source_dir, img_format):
    images_per_class = {}

    for root, dirs, files in os.walk(source_dir):
        for file_ in files:
            if not os.path.splitext(file_)[-1] != img_format:
                print("Skipping {0}".format(file_))
                continue
            file_path = os.path.join(root, file_)
            class_label = os.path.basename(root)
            # ToDo: Fix this dirty hack
            #image_bgr = cv2.imread(file_path, cv2.IMREAD_COLOR)
            images_per_class[os.path.join(class_label, file_)] = None
    print("all images saved in array")
    return images_per_class