# load_model_sample.py
from keras.models import load_model
from keras.preprocessing import image
from keras.applications import VGG16
import keras.applications.mobilenet as mobilenet
import matplotlib.pyplot as plt
import numpy as np
from personal_config import TEST_DIR
import os


def load_image(img_path,
               img_width=150,
               img_height=150,
               rescaling_factor=255.0,
               show=False):
    img = image.load_img(img_path, target_size=(img_height, img_width))
    img_tensor = image.img_to_array(img)  # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)  # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= rescaling_factor  # imshow expects values in the range [0, 1]

    if show:
        plt.imshow(img_tensor[0])
        plt.axis('off')
        plt.show()

    return img_tensor


def predict_binary(h5_path, picture_array, custom_metrics, img_width=150, img_height=150):
    custom_objects = dict()
    for item in custom_metrics:
        custom_objects[item.__name__] = item

    if 'mobilenet' in h5_path:
        print("Adding Mobilenet Objects...")
        custom_objects['relu6'] = mobilenet.relu6
        custom_objects['DepthwiseConv2D'] = mobilenet.DepthwiseConv2D

    # load model
    model = load_model(h5_path, custom_objects=custom_objects)
    result_array = []
    for pic_path in picture_array:
        # load a single image
        new_image = load_image(pic_path, img_width=img_width, img_height=img_height)
        # check prediction
        pred = model.predict(new_image)
        result_array.append((pic_path, [0 if pred.max() < 0.5 else 1], pred.max()))
    return result_array


def predict(h5_path, picture_array, custom_metrics, img_width=150, img_height=150):
    custom_objects = dict()
    for item in custom_metrics:
        custom_objects[item.__name__] = item

    if 'mobilenet' in h5_path:
        print("Adding Mobilenet Objects...")
        custom_objects['relu6'] = mobilenet.relu6
        custom_objects['DepthwiseConv2D'] = mobilenet.DepthwiseConv2D

    # load model
    model = load_model(h5_path, custom_objects=custom_objects)
    result_array = []
    for pic_path in picture_array:
        # load a single image
        new_image = load_image(pic_path, img_width=img_width, img_height=img_height)
        # check prediction
        pred = model.predict(new_image)
        result_array.append((pic_path, pred.argmax(axis=-1), pred.max()))
    return result_array


def predict_pretrained(h5_path,
                       picture_array,
                       custom_metrics,
                       conv_base=VGG16(weights='imagenet', include_top=False, input_shape=(150, 150, 3)),
                       reshape=(1, 4*4*512)):

    custom_objects = dict()
    for item in custom_metrics:
        custom_objects[item.__name__] = item
    # load model
    model = load_model(h5_path, custom_objects=custom_objects)
    result_array = []
    for pic_path in picture_array:
        # load a single image
        new_image = load_image(pic_path)
        # conv base prediction
        img_features = conv_base.predict(new_image)
        img_features = np.reshape(img_features, reshape)
        # check prediction
        pred = model.predict(img_features)
        result_array.append((pic_path, pred.argmax(axis=-1), pred.max()))
    return result_array


def read_class_label(label_txt_path, reverse=False):
    return_dict = dict()
    with open(label_txt_path, 'r') as fi:
        for line in fi.readlines():
            data = line.split(": ")
            if reverse:
                return_dict[data[0]] = int(data[1])
            else:
                return_dict[int(data[1])] = data[0]
    return return_dict


def test_data_array(test_folder=TEST_DIR):
    return [os.path.join(test_folder, file_path) for file_path in os.listdir(test_folder)]

if __name__ == '__main__':
    pass