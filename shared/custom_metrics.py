from keras import backend as K


# Adapted from http://forums.fast.ai/t/fmeasure-in-keras/1252/2

def recall(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    return true_positives / (possible_positives + K.epsilon())


def precision(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))

    return true_positives / (predicted_positives + K.epsilon())


def fmeasure(y_true, y_pred, beta=1):
    # Calculates the f-measure, the harmonic mean of precision and recall.
    rec = recall(y_true=y_true, y_pred=y_pred)
    prec = precision(y_true=y_true, y_pred=y_pred)

    bb = beta ** 2

    fbeta_score = (1 + bb) * (prec * rec) / (bb * prec + rec + K.epsilon())
    return fbeta_score



