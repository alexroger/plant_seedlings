import os
import shutil
import numpy as np
import pandas as pd
from personal_config import CATEGORIES, TRAIN_DIR, DATA


def get_amount_of_files(folder):
    if not os.path.isdir(folder):
        raise Exception("{0} is not a directory!".format(folder))
    return sum([len(files) for r, d, files in os.walk(folder)])


def copy_pictures_arrays(training_data,
                         validation_data,
                         source_train_directory=os.path.join(DATA, 'train'),
                         target_train_directory=os.path.join(DATA, 'exec_train'),
                         target_validation_directory=os.path.join(DATA, 'exec_validation')):

    print("Checking Training Data Folder Structure...")
    if os.path.isdir(target_train_directory):
        print("Deleting {0}...".format(target_train_directory))
        shutil.rmtree(target_train_directory)
    if not os.path.isdir(target_train_directory):
        os.mkdir(target_train_directory)
    print("Checking Validation Data Folder Structure...")
    if os.path.isdir(target_validation_directory):
        print("Deleting {0}...".format(target_validation_directory))
        shutil.rmtree(target_validation_directory)
    if not os.path.isdir(target_validation_directory):
        os.mkdir(target_validation_directory)

    # Moving the files to the correct folders
    print("Copying {0} Training Files...".format(len(training_data)))
    copy_to_folders(training_data,
                    source_base_dir=source_train_directory,
                    target_base_dir=target_train_directory)

    print("Copying {0} Validation Files...".format(len(validation_data)))
    copy_to_folders(validation_data,
                    source_base_dir=source_train_directory,
                    target_base_dir=target_validation_directory)

    # Returning Validation Split in the Form (train, validation)
    return len(training_data), len(validation_data)


def train_val_split_on_folder(source_directory=TRAIN_DIR,
                              train_size=0.8):
    print("Create Training Sample from {0}...".format(source_directory))
    train = []
    for category_id, category in enumerate(CATEGORIES):
        for file in os.listdir(os.path.join(source_directory, category)):
            train.append(['{}/{}'.format(category, file), category_id, category])
    train = pd.DataFrame(train, columns=['file', 'category_id', 'category'])
    print("Generated DataFrame with shape ", train.shape)

    # TODO: should be validated with copied images?
    print("Creating Validation Split...")
    rnd = np.random.random(len(train))
    train_idx = rnd < train_size
    valid_idx = rnd >= train_size
    # Create Lists based on Split
    train_files = train.loc[train_idx, 'file'].values
    validation_files = train.loc[valid_idx, 'file'].values
    return train_files, validation_files


def copy_to_folders(array, source_base_dir, target_base_dir):
    for item in array:
        full_path = os.path.normpath(os.path.join(source_base_dir, item))
        category = os.path.basename(os.path.dirname(full_path))
        cat_dir = os.path.join(target_base_dir, category)
        if not os.path.isfile(full_path):
            print("File {0} does not exist!".format(full_path))
            raise Exception("Wrong Path!")
        if not os.path.isdir(cat_dir):
            os.mkdir(cat_dir)

        target_path = os.path.join(os.path.join(target_base_dir, category), os.path.basename(full_path))
        if not os.path.exists(target_path):
            #print("Copying ", full_path, " to ", target_path)
            shutil.copy2(full_path, target_path)

if __name__ == '__main__':
    print("Copying Pictures")
    #len_ytr, len_yv = copy_pictures()
    #print(len_ytr, len_yv)