from datetime import datetime as dt
import socket
import sys
import os


class Timer(object):

    def __init__(self, name):
        self.name = name
        self.start = dt.now()
        self.end = None

    def finish(self):
        self.end = dt.now()

    def minutes(self):
        if self.end is None:
            self.finish()
        return round((self.end - self.start).seconds / 60, ndigits=2)

    def __repr__(self):
        return 'Elapsed time for {name} was {time} minutes.'.format(name=self.name,
                                                                    time=self.minutes())


def create_run_folder(model_name):
    model_id = "{model}_{host}_{time}".format(model=model_name.replace('.', '-'),
                                              host=socket.gethostname(),
                                              time=dt.now().strftime("%Y-%m-%d-%H-%M"))

    model_folder = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),
                                os.path.join('runs', model_id))

    if os.path.isdir(model_folder):
        raise Exception('Model Save Path already exists!')
    assert not os.path.isdir(model_folder)
    os.makedirs(model_folder)
    return model_folder


def generate_csv(folder, result, label_dict):
    print("Generating CSVs...")
    with open(os.path.join(folder, 'prediction.csv'), 'w') as po:
        with open(os.path.join(folder,'submission.csv'), 'w') as subo:
            po.write('Item,Label,Prob\n')
            subo.write('file,species\n')
            for item, position, pred_prob in result:
                po.write("{0},{1},{2}\n".format(os.path.basename(item), label_dict[position[0]], pred_prob))
                subo.write("{0},{1}\n".format(os.path.basename(item), label_dict[position[0]]))
    print("CSV generated")


def class_label_generator(classes, target):
    # Getting Class Labels
    with open(target, 'w') as label_file:
        try:
            if isinstance(classes, dict):
                print("Got a Dict for Class Labels.")
                for key in classes.keys():
                    label_file.write("{0}: {1}\n".format(key, classes[key]))
                return classes
            elif os.path.isdir(classes):
                print("Got a Directory for Class Labels. Assuming Subfolders are labels...")
                class_labels = dict()
                for i, category in enumerate(os.listdir(classes)):
                    label_file.write("{0}: {1}\n".format(category, i))
                    class_labels[category] = i
                return class_labels

            else:
                raise Exception("Got invalid Input to class_label_generator!")
        except ValueError as ex:
            raise ex

