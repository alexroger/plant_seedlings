import matplotlib.pyplot as plt
import sklearn.metrics
import numpy as np
import itertools
from shared import predict
import mpl_toolkits.axes_grid1
import os
import simplejson as json
import cv2
from shared.predict import read_class_label
from keras import metrics
import shared.custom_metrics as custom_metrics
from shared.image_Morphological import  process_image


def plot_training_validation_metric(history, metric, fig_path):
    print("Generating Plot of {0}".format(metric))
    history_dict = history.history
    values = history_dict[metric]
    validation_values = history_dict['val_{0}'.format(metric)]

    epochs = range(1, len(values) + 1)
    fig = plt.figure()
    plt.plot(epochs, values, 'bo', label='Training {0}'.format(metric), figure=fig)
    plt.plot(epochs, validation_values, 'b', label='Validation {0}'.format(metric), figure=fig)
    plt.title('Training and validation {0}'.format(metric))
    plt.xlabel('Epochs')
    plt.ylabel('{0}'.format(metric))
    plt.legend()
    plt.savefig(fig_path)


def plot_training_validation_loss(run_path):
    history_json = os.path.join(run_path, r'history.json')
    loss_fig = os.path.join(run_path, r'loss.png')
    metric = 'loss'

    with open(history_json) as fi:
        history_dict = json.loads(fi.read())

    print("Generating Plot of {0}".format(metric))
    values = history_dict[metric]
    validation_values = history_dict['val_{0}'.format(metric)]

    epochs = range(1, len(values) + 1)
    fig = plt.figure()
    plt.plot(epochs, values, 'bo', label='Training {0}'.format(metric), figure=fig)
    plt.plot(epochs, validation_values, 'b', label='Validation {0}'.format(metric), figure=fig)
    plt.title('Training and validation {0}'.format(metric))
    plt.xlabel('Epochs')
    plt.ylabel('{0}'.format(metric))
    plt.legend()
    plt.savefig(loss_fig)


def plot_confusion_matrix(confusion_matrix,
                          class_array,
                          fig_path,
                          title="Confusion Matrix",
                          cmap=plt.cm.Blues):
    # Adapted from http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    fig = plt.figure(figsize=(10, 10))
    plt.imshow(confusion_matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(class_array))
    plt.xticks(tick_marks, class_array, rotation=90)
    plt.yticks(tick_marks, class_array)

    thresh = confusion_matrix.max() / 2.
    for i, j in itertools.product(range(confusion_matrix.shape[0]), range(confusion_matrix.shape[1])):
        plt.text(j, i, confusion_matrix[i, j],
                 horizontalalignment="center",
                 color="white" if confusion_matrix[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(fig_path)


def plot_image_matrix(training_data, species, image_per_species):
    # Adapted from: https://www.kaggle.com/raoulma/plants-xception-90-06-test-accuracy/notebook
    # Quick Hack for Paper
    num_species = len(species)
    fig = plt.figure(1, figsize=(num_species, image_per_species))
    grid = mpl_toolkits.axes_grid1.ImageGrid(fig, rect=111, nrows_ncols=(num_species, image_per_species),
                                             axes_pad=0.05)
    i = 0
    for species_id, sp in enumerate(species):
        folder = os.path.join(training_data, sp)
        print("Processing {0}".format(folder))
        for file_ in os.listdir(folder)[:image_per_species]:
            file_path = os.path.join(folder, file_)
            ax = grid[i]
            img = cv2.imread(file_path, cv2.IMREAD_COLOR)
            img = cv2.resize(img.copy(), (150, 150), interpolation=cv2.INTER_AREA)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            #img = process_image(img)
            ax.imshow(img)  # matplotlib uses rgb, not bgr
            ax.axis('off')
            if i % image_per_species == image_per_species - 1:
                ax.text(250, 111, sp, verticalalignment='center')
            i += 1
    plt.show()


if __name__ == '__main__':
    #species = ['Black-grass', 'Charlock', 'Cleavers', 'Common Chickweed', 'Common wheat',
    #           'Fat Hen', 'Loose Silky-bent', 'Maize', 'Scentless Mayweed',
     #          'Shepherds Purse', 'Small-flowered Cranesbill', 'Sugar beet']

    # Plot for baseline model
    #species = ['Black-grass', "Common wheat", "Loose Silky-bent"]

    #plot_image_matrix(r'C:\Data\plant_seedlings\train', species=species, image_per_species=10)
    plot_training_validation_loss(r"E:\Code\plant_seedlings\runs\pretrained-model2_mobilenet_RogerDesktop_2018-03-10-17-41")
