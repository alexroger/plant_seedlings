import os
import keras.metrics as metrics

import shared.custom_metrics as custom_metrics
from shared.plotting import *
import shared.utility
import shared.predict

general_model = r'E:\Code\plant_seedlings\runs\pretrained-model2_mobilenet_RogerDesktop_2018-03-10-17-41'
special_model = r'E:\Code\plant_seedlings\runs\pretrained-model2_mobilenet_special_RogerDesktop_2018-03-11-12-02'

combine_folder = r'E:\Code\plant_seedlings\runs\combined_folder'
if not os.path.isdir(combine_folder):
    os.makedirs(combine_folder)

general_model_path = os.path.join(general_model, 'model.h5')
special_model_path = os.path.join(special_model, 'model.h5')

general_validation_files = os.path.join(general_model, 'validation_files.txt')

general_evaluation_metrics = [metrics.categorical_accuracy,
                              custom_metrics.fmeasure,
                              custom_metrics.precision,
                              custom_metrics.recall]

special_evaluation_metrics = [metrics.binary_accuracy,
                              custom_metrics.fmeasure,
                              custom_metrics.precision,
                              custom_metrics.recall]

general__validation_array = []
general_class_array = []
general_label_array = []
general_class_dict = read_class_label(label_txt_path=os.path.join(general_model, 'class_labels.txt'),
                                      reverse=True)

general_predictions = predict.predict(h5_path=general_model_path,
                                      picture_array=shared.predict.test_data_array(),
                                      custom_metrics=general_evaluation_metrics,
                                      img_width=160,
                                      img_height=160)

new_picture_array = [item[0] for item in general_predictions if item[1].tolist()[0] in [0, 6] and item[2] < 0.9]  # Black Grass and Loose Silky Bent

special_predictions = predict.predict_binary(h5_path=special_model_path,
                                             picture_array=new_picture_array,
                                             custom_metrics=special_evaluation_metrics,
                                             img_width=160,
                                             img_height=160)


def get_binary_element(file_name, binary_predictions):
    for item in binary_predictions:
        if item[0] == file_name:
            print(item)
            result = None
            if item[1][0] == 0:  # Black-Grass
                result = [0]
            elif item[1][0] == 1:  # Loose Silky Bent
                result = [6]
            else:
                raise Exception("Wrong Number")
            return result

combined_predictions = list()

for item in general_predictions:
    if item[0] in new_picture_array:
        new_element = (item[0], get_binary_element(item[0], special_predictions), item[2])
        combined_predictions.append(new_element)
    else:
        combined_predictions.append(item)

for item in combined_predictions:
    print(item)

shared.utility.generate_csv(folder=combine_folder,
                            result=combined_predictions,
                            label_dict=read_class_label(label_txt_path=os.path.join(general_model, 'class_labels.txt')))

"""
validation_array = []
class_array = []
label_array = []
class_dict = read_class_label(label_txt_path=os.path.join(general_model, 'class_labels.txt'),
                              reverse=True)

with open(general_validation_files, 'r') as fi:
    for item in fi.readlines():
        couple = [item.strip() for item in item.split(r'/')]
        validation_array.append(os.path.join(r'C:\Data\plant_seedlings\train\{0}'.format(couple[0]), couple[1]))
        class_array.append(class_dict[couple[0]])
        if couple[0] not in label_array:
            label_array.append(couple[0])

predictions = [item[1][0] for item in combined_predictions]
confusionMTX = sklearn.metrics.confusion_matrix(class_array, predictions)
print(confusionMTX)

plot_confusion_matrix(confusion_matrix=confusionMTX,
                      class_array=label_array,
                      fig_path=os.path.join(combine_folder, 'confusion_matrix.png'))

"""
