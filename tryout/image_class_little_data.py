# Adapted from https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html

import os

"""
A message that I hear often is that "deep learning is only relevant when you have a huge amount of data". While not
entirely incorrect, this is somewhat misleading. Certainly, deep learning requires the ability to learn features
automatically from the data, which is generally only possible when lots of training data is available --
especially for problems where the input samples are very high-dimensional, like images. However, convolutional
neural networks --a pillar algorithm of deep learning-- are by design one of the best models available for most
"perceptual" problems (such as image classification), even with very little data to learn from. Training a convnet
from scratch on a small image dataset will still yield reasonable results, without the need for any custom feature
engineering. Convnets are just plain good. They are the right tool for the job.

But what's more, deep learning models are by nature highly repurposable: you can take, say, an image classification
or speech-to-text model trained on a large-scale dataset then reuse it on a significantly different problem with only
minor changes, as we will see in this post. Specifically in the case of computer vision, many pre-trained models
(usually trained on the ImageNet dataset) are now publicly available for download and can be used to bootstrap powerful
vision models out of very little data.
"""

# Constants
DATA = r'E:\Data\cats_dogs'

#########################################################################################################
# Generating new images Illustration                                                                    #
# Image Data Generator Documentation: https://keras.io/preprocessing/image/                             #
#########################################################################################################
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

datagen = ImageDataGenerator(
    rotation_range=40,  # degree for rotation
    width_shift_range=0.2,  # fraction of total width/height "within which to randomly translate pictures vertically or horizontally
    height_shift_range=0.2,
    #rescale=1./255,  # scaling the 255 RGB Values to between 0 and 1 because 255 are to high for model
    shear_range=0.2,  # randomly applying shearing transformations, see https://en.wikipedia.org/wiki/Shear_mapping
    zoom_range=0.2,  # randomly zooming inside pictures
    horizontal_flip=True,  # randomly flipping half of the images horizontally
    fill_mode='nearest'  # filling newly created pixels strategy (because of width height shift necessary)
)


img = load_img(os.path.join(DATA, r'train/cats/cat.500.jpg'))
x = img_to_array(img)  # Numpy Array with shape (3, 150, 150) = 3 Channels, 150 Width, 150 Height
x = x.reshape((1,) + x.shape)

DIR_TO_SAVE = os.path.join(DATA, 'preview')
if not os.path.isdir(DIR_TO_SAVE):
    os.mkdir(DIR_TO_SAVE)

i = 0
for batch in datagen.flow(x, batch_size=1, save_to_dir=DIR_TO_SAVE, save_prefix='cat', save_format='jpeg'):
    i+= 1
    if i > 20:
        break

#########################################################################################################
# Real Model                                                                                            #
# See https://gist.github.com/fchollet/0830affa1f7f19fd47b06d4cf89ed44d for the full code               #
#########################################################################################################
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K

# dimensions of our images.
img_width, img_height = 150, 150

train_data_dir = os.path.join(DATA, 'train')
validation_data_dir = os.path.join(DATA, 'validation')
nb_train_samples = 2000
nb_validation_samples = 800
epochs = 50
batch_size = 16

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

# Note: Labels are set by flow_from_directory based on folder structure if not supplied
train_generator = train_datagen.flow_from_directory(
    train_data_dir,  # this is the target directory
    target_size=(img_width, img_height),  # all images will be resized to 150x150
    batch_size=batch_size,
    class_mode='binary')  # since we use binary_crossentropy loss, we need binary labels

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='binary')

model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size)

model.save_weights('first_try.h5')

