from keras.models import load_model
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np
from personal_config import TEST_DIR
from shared.predict import load_image
from keras import metrics
import shared.custom_metrics as custom_metrics
from keras.applications import VGG16

h5_path = r"C:\Users\Alex\PycharmProjects\plant_seedlings\runs\pretrained-model0_DESKTOP-1L7RCSM_2018-02-25-13-40\model.h5"
testimg = r"C:\Users\Alex\Documents\CAS_PML\Semesterarbeit\RowData\test\0021e90e4.png"

evaluation_metrics = [metrics.categorical_accuracy,
                      custom_metrics.fmeasure,
                      custom_metrics.precision,
                      custom_metrics.recall]

conv_base = VGG16(weights='imagenet', include_top=False, input_shape=(150,150,3))

# features = np.zeros(shape=(1, 4, 4, 512))  # last Layer from pretrained model
features_batch = conv_base.predict(load_image(testimg))

test_features = np.reshape(features_batch, (1,4*4*512))


custom_objects = dict()
for item in evaluation_metrics:
    custom_objects[item.__name__] = item

# load model
model = load_model(h5_path,custom_objects = custom_objects)

print(model.summary())
print(model.predict(test_features))



