import os
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

DATA_FOLDER = r'C:\Data\plant_seedlings'
train_data = os.path.join(DATA_FOLDER, r'train')
SHOW_IMAGES = True

datagen = ImageDataGenerator(
    rotation_range=40,  # degree for rotation
    width_shift_range=0.2,  # fraction of total width/height "within which to randomly translate pictures vertically or horizontally
    height_shift_range=0.2,
    shear_range=0.2,  # randomly applying shearing transformations, see https://en.wikipedia.org/wiki/Shear_mapping
    zoom_range=0.2,  # randomly zooming inside pictures
    horizontal_flip=True,  # randomly flipping half of the images horizontally
    fill_mode='nearest'  # filling newly created pixels strategy (because of width height shift necessary)
)

img = load_img(os.path.join(train_data, r'Common Chickweed\00b6eee9f.png'))
x = img_to_array(img)  # Numpy Array with shape (3, 150, 150) = 3 Channels, 150 Width, 150 Height
x = x.reshape((1,) + x.shape)

DIR_TO_SAVE = os.path.join(DATA_FOLDER, 'preview')
if not os.path.isdir(DIR_TO_SAVE):
    os.mkdir(DIR_TO_SAVE)

i = 0
for batch in datagen.flow(x, batch_size=1, save_to_dir=DIR_TO_SAVE, save_prefix='cat', save_format='jpeg'):
    i+= 1
    if i > 20:
        break