import os
from personal_config import DATA

DATA_PATH = os.path.join(DATA, 'train')

counts = dict()

for root, dirs, files in os.walk(DATA_PATH):
    for file_ in files:
        element = os.path.basename(os.path.normpath(root))
        counts[element] = counts.get(element, 0) + 1

for key, value in counts.items():
    print (key, value)

