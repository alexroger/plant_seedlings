import glob
import os
from PIL import Image, ImageOps
import numpy as np
from scipy.misc import imresize
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
import personal_config

WIDTH_HEIGHT = 128

ori_label = []
ori_imgs = []
for root, dirs, files in os.walk(personal_config.EXEC_TRAIN_DIR):
    for file_ in files:
        if os.path.splitext(file_)[-1] != '.png':
            continue
        file_path = os.path.join(root, file_)
        ori_label.append(os.path.basename(os.path.dirname(file_path)))
        new_img = Image.open(file_path)
        ori_imgs.append(ImageOps.fit(new_img, (WIDTH_HEIGHT, WIDTH_HEIGHT), Image.ANTIALIAS).convert('RGB'))

imgs = np.array([np.array(im) for im in ori_imgs])
imgs = imgs.reshape(imgs.shape[0], WIDTH_HEIGHT, WIDTH_HEIGHT, 3) / 255.0
lb = LabelBinarizer().fit(ori_label)
label = lb.transform(ori_label)

trainX, validX, trainY, validY = train_test_split(imgs, label, test_size=0.05, random_state=42)

from keras.layers import Dropout, Input, Dense, Activation,GlobalMaxPooling2D, BatchNormalization, Flatten, Conv2D, MaxPooling2D
from keras.models import Model, load_model
from keras.optimizers import Adam


IM_input = Input((WIDTH_HEIGHT, WIDTH_HEIGHT, 3))
IM = Conv2D(16, (3, 3))(IM_input)
IM = BatchNormalization(axis = 3)(IM)
IM = Activation('relu')(IM)
IM = Conv2D(16, (3, 3))(IM)
IM = BatchNormalization(axis = 3)(IM)
IM = Activation('relu')(IM)
IM = MaxPooling2D((2, 2), strides=(2, 2))(IM)
IM = Conv2D(32, (3, 3))(IM)
IM = BatchNormalization(axis = 3)(IM)
IM = Activation('relu')(IM)
IM = Conv2D(32, (3, 3))(IM)
IM = BatchNormalization(axis = 3)(IM)
IM = Activation('relu')(IM)
IM = GlobalMaxPooling2D()(IM)
IM = Dense(64, activation='relu')(IM)
IM = Dropout(0.5)(IM)
IM = Dense(32, activation='relu')(IM)
IM = Dropout(0.5)(IM)
IM = Dense(12, activation='softmax')(IM)
model = Model(inputs=IM_input, outputs=IM)
model.summary()
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(lr=1e-4), metrics=['acc'])

from keras.callbacks import LearningRateScheduler, EarlyStopping
from keras.callbacks import ModelCheckpoint

batch_size = 64
annealer = LearningRateScheduler(lambda x: 1e-3 * 0.9 ** x)
earlystop = EarlyStopping(patience=10)
modelsave = ModelCheckpoint(
    filepath='model.h5',
    save_best_only=True, verbose=1)

model.fit(
    trainX,
    trainY,
    batch_size=batch_size,
    epochs=200,
    validation_data=(validX, validY),
    callbacks=[annealer, earlystop, modelsave]
)


test_imgs = []
names = []
for root, dirs, files in os.walk(personal_config.EXEC_VAL_DIR):
    for file_ in files:
        if os.path.splitext(file_)[-1] != '.png':
            continue
        file_path = os.path.join(root, file_)
        names.append(file_)
        new_img = Image.open(file_path)
        test_img = ImageOps.fit(new_img, (WIDTH_HEIGHT, WIDTH_HEIGHT), Image.ANTIALIAS).convert('RGB')
        test_imgs.append(test_img)

model = load_model('model.h5')

timgs = np.array([np.array(im) for im in test_imgs])
testX = timgs.reshape(timgs.shape[0], WIDTH_HEIGHT, WIDTH_HEIGHT, 3) / 255.0

yhat = model.predict(testX)
test_y = lb.inverse_transform(yhat)

import pandas as pd

df = pd.DataFrame(data={'file': names, 'species': test_y})
df_sort = df.sort_values(by=['file'])
df_sort.to_csv('results.csv', index=False)
