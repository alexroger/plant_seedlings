from keras import backend as K
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from keras import losses

from shared.picture_copy import *
from shared.image_preprocessing import vectorize_images_in_folder
import shared.utility
from keras.callbacks import TensorBoard

from shared.image_preprocessing import LoadImagestoArray
from shared.image_Morphological import prepImage

# Configuration Hierachy
import personal_config
from models.baseline.baseline_conf import *
from models.baseline.model2c.configuration import *


def preProcessing_img():
    imgArray = LoadImagestoArray(personal_config.TRAIN_DIR, "png")
    prepImage(PREPROCESS_TRAIN_DIR, imgArray)
    imgArray = LoadImagestoArray(personal_config.TEST_DIR, "png")
    prepImage(PREPROCESS_TEST_DIR, imgArray)
    personal_config.TEST_DIR = PREPROCESS_TEST_DIR
    personal_config.TRAIN_DIR = PREPROCESS_TRAIN_DIR
    return personal_config.TRAIN_DIR, personal_config.TEST_DIR


def prepare_data(source_directory=TRAIN_DIR,
                 save_training_data=None,
                 save_validation_data=None):
    # Validation Split
    training_data, validation_data = train_val_split_on_folder(source_directory=source_directory,
                                                               train_size=0.8)

    if save_training_data:
        with open(save_training_data, 'w') as td:
            td.write('\n'.join(training_data))
    if save_validation_data:
        with open(save_validation_data, 'w') as tv:
            tv.write('\n'.join(validation_data))

    copy_pictures_arrays(training_data=training_data,
                         validation_data=validation_data,
                         source_train_directory=source_directory,
                         target_train_directory=EXEC_TRAIN_DIR,
                         target_validation_directory=EXEC_VAL_DIR)


def train_model(model_save_path,
                class_labels_path,
                evaluation_metrics,
                debug_path,
                epochs,
                early_stop=True,
                best_model=True):

    # MODEL DEFINITION
    if K.image_data_format() == 'channels_first':
        input_shape = (3, IMG_WIDTH, IMG_HEIGHT)
    else:
        input_shape = (IMG_WIDTH, IMG_HEIGHT, 3)

    model = Sequential()
    model.add(Conv2D(32, (2, 2), input_shape=input_shape))
    model.add(Activation('tanh'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (2, 2)))
    model.add(Activation('tanh'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (2, 2)))
    model.add(Activation('tanh'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('tanh'))
    model.add(Dropout(0.3))

    model.add(
        Dense(12))  # Since we have 12 classes we need a probability distribution over these 12 labels using softmax
    model.add(Activation('softmax'))

    model.compile(loss=losses.categorical_crossentropy,
                  optimizer='rmsprop',
                  metrics=evaluation_metrics)

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    val_datagen = ImageDataGenerator(rescale=1. / 255)

    # Count the number of files
    train_samples = get_amount_of_files(EXEC_TRAIN_DIR)
    val_samples = get_amount_of_files(EXEC_VAL_DIR)
    print("Training/Validation Split is: {0} - {1}".format(train_samples, val_samples))

    # Note: Labels are set by flow_from_directory based on folder structure if not supplied
    train_generator = train_datagen.flow_from_directory(
        EXEC_TRAIN_DIR,  # this is the target directory
        target_size=(IMG_WIDTH, IMG_HEIGHT),  # all images will be resized to 150x150
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    # Saving Class Labels
    shared.utility.class_label_generator(train_generator.class_indices, class_labels_path)

    validation_generator = val_datagen.flow_from_directory(
        EXEC_VAL_DIR,
        target_size=(IMG_WIDTH, IMG_HEIGHT),
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    callbacks = []
    if early_stop:
        callbacks.append(EarlyStopping(patience=10))
    callbacks.append(ModelCheckpoint(filepath=model_save_path,
                                     save_best_only=best_model,
                                     verbose=1)
                     )
    callbacks.append(TensorBoard(log_dir=debug_path,
                                 histogram_freq=0,
                                 write_graph=True,
                                 write_images=True,
                                 write_grads=True ))

    history = model.fit_generator(
              train_generator,
              steps_per_epoch=train_samples // BATCH_SIZE,
              epochs=epochs,
              callbacks=callbacks,
              validation_data=validation_generator,
              validation_steps=val_samples // BATCH_SIZE,)

    return history
