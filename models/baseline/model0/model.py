from keras import backend as K
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras import losses

from shared.picture_copy import *
from shared.image_preprocessing import vectorize_images_in_folder
import shared.utility

from shared.image_preprocessing import LoadImagestoArray
from shared.image_Morphological import prepImage

# Configuration Hierachy
import personal_config
from models.baseline.baseline_conf import *
from models.baseline.model0.configuration import *


def preProcessing_img():
    imgArray = LoadImagestoArray(personal_config.TRAIN_DIR, "png")
    prepImage(PREPROCESS_TRAIN_DIR, imgArray)
    imgArray = LoadImagestoArray(personal_config.TEST_DIR, "png")
    prepImage(PREPROCESS_TEST_DIR, imgArray)
    personal_config.TEST_DIR = PREPROCESS_TEST_DIR
    personal_config.TRAIN_DIR = PREPROCESS_TRAIN_DIR


def prepare_data(save_training_data=None,
                 save_validation_data=None):
    training_directory = personal_config.TRAIN_DIR

    training_data, validation_data = train_val_split_on_folder(source_directory=training_directory,
                                                               train_size=0.8)

    if save_training_data:
        with open(save_training_data, 'w') as td:
            td.write('\n'.join(training_data))
    if save_validation_data:
        with open(save_validation_data, 'w') as tv:
            tv.write('\n'.join(validation_data))

    copy_pictures_arrays(training_data=training_data,
                         validation_data=validation_data,
                         target_train_directory=EXEC_TRAIN_DIR,
                         target_validation_directory=EXEC_VAL_DIR)


# generatePicture
#generatePicture(train_dir=EXEC_TRAIN_DIR, numberof=400)


def train_model(model_save_path,
                class_labels_path,
                evaluation_metrics,
                debug_path,
                epochs,
                early_stop=True,
                best_model=True):


    # MODEL DEFINITION
    if K.image_data_format() == 'channels_first':
        input_shape = (3, IMG_WIDTH, IMG_HEIGHT)
    else:
        input_shape = (IMG_WIDTH, IMG_HEIGHT, 3)

    # Saving Class Labels
    class_labels = shared.utility.class_label_generator(EXEC_TRAIN_DIR, class_labels_path)

    train_images, train_labels = vectorize_images_in_folder(EXEC_TRAIN_DIR,
                                                            target_width=IMG_WIDTH,
                                                            target_height=IMG_HEIGHT,
                                                            class_label_dict=class_labels)
    val_images, val_labels = vectorize_images_in_folder(EXEC_VAL_DIR,
                                                        target_width=IMG_WIDTH,
                                                        target_height=IMG_HEIGHT,
                                                        class_label_dict=class_labels)

    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))

    model.add(Dense(12))  # Since we have 12 classes we need a probability distribution over these 12 labels using softmax
    model.add(Activation('softmax'))

    model.compile(loss=losses.categorical_crossentropy,
                  optimizer='rmsprop',
                  metrics=evaluation_metrics)

    callbacks = []
    if early_stop:
        callbacks.append(EarlyStopping(patience=10))
    callbacks.append(ModelCheckpoint(filepath=model_save_path,
                                     save_best_only=best_model,
                                     verbose=1))

    history = model.fit(x=train_images,
                        y=train_labels,
                        batch_size=BATCH_SIZE,
                        epochs=epochs,
                        validation_data=(val_images, val_labels),
                        callbacks=callbacks)

    return history
