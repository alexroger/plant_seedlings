from keras import backend as K
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras import losses

from shared.picture_copy import *
import shared.utility

# Configuration Hierachy
from personal_config import *
from models.baseline.baseline_conf import *
from models.baseline.model2b.configuration import *


def prepare_data(save_training_data=None,
                 save_validation_data=None):
    # Validation Split
    training_data, validation_data = train_val_split_on_folder(source_directory=TRAIN_DIR,
                                                               train_size=0.7)

    if save_training_data:
        with open(save_training_data, 'w') as td:
            td.write('\n'.join(training_data))
    if save_validation_data:
        with open(save_validation_data, 'w') as tv:
            tv.write('\n'.join(validation_data))

    copy_pictures_arrays(training_data=training_data,
                         validation_data=validation_data,
                         target_train_directory=EXEC_TRAIN_DIR,
                         target_validation_directory=EXEC_VAL_DIR)


def train_model(model_save_path,
                class_labels_path,
                evaluation_metrics,
                debug_path,
                epochs,
                early_stop=True,
                best_model=True):


    # MODEL DEFINITION
    if K.image_data_format() == 'channels_first':
        input_shape = (3, IMG_WIDTH, IMG_HEIGHT)
    else:
        input_shape = (IMG_WIDTH, IMG_HEIGHT, 3)

    model = Sequential()
    model.add(Conv2D(36, (3, 3), input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(36, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(72, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(72, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(72))
    model.add(Activation('relu'))
    model.add(Dropout(0.4))

    model.add(
        Dense(12))  # Since we have 12 classes we need a probability distribution over these 12 labels using softmax
    model.add(Activation('softmax'))

    model.compile(loss=losses.categorical_crossentropy,
                  optimizer='rmsprop',
                  metrics=evaluation_metrics)

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    val_datagen = ImageDataGenerator(rescale=1. / 255)

    # Count the number of files
    train_samples = get_amount_of_files(EXEC_TRAIN_DIR)
    val_samples = get_amount_of_files(EXEC_VAL_DIR)
    print("Training/Validation Split is: {0} - {1}".format(train_samples, val_samples))

    # Note: Labels are set by flow_from_directory based on folder structure if not supplied
    train_generator = train_datagen.flow_from_directory(
        EXEC_TRAIN_DIR,  # this is the target directory
        target_size=(IMG_WIDTH, IMG_HEIGHT),  # all images will be resized to 150x150
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    # Saving Class Labels
    shared.utility.class_label_generator(train_generator.class_indices, class_labels_path)

    validation_generator = val_datagen.flow_from_directory(
        EXEC_VAL_DIR,
        target_size=(IMG_WIDTH, IMG_HEIGHT),
        batch_size=BATCH_SIZE,
        class_mode='categorical')

    callbacks = []
    if early_stop:
        callbacks.append(EarlyStopping(patience=15))
    callbacks.append(ModelCheckpoint(filepath=model_save_path,
                                     save_best_only=best_model,
                                     verbose=1))

    history = model.fit_generator(
              train_generator,
              steps_per_epoch=train_samples // BATCH_SIZE,
              epochs=epochs,
              callbacks=callbacks,
              validation_data=validation_generator,
              validation_steps=val_samples // BATCH_SIZE)

    return history
