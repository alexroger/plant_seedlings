import os
from personal_config import DATA


PREPROCESS_DIR = os.path.join(DATA, '{0}_preprocess'.format(os.path.basename(os.path.dirname(__file__))))
EXEC_TRAIN_DIR = os.path.join(DATA, '{0}_train'.format(os.path.basename(os.path.dirname(__file__))))
EXEC_VAL_DIR = os.path.join(DATA, '{0}_val'.format(os.path.basename(os.path.dirname(__file__))))

BATCH_SIZE = 64