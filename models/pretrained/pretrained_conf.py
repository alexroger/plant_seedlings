IMG_WIDTH, IMG_HEIGHT = 160, 160
BATCH_SIZE = 24

# Mobilenet: 128x128
# Xception: 72x72

# For mobilenet only [128, 160, 192, 224] are valid
# 2 GB VRAM = 128x128
# see https://github.com/keras-team/keras/issues/8090
