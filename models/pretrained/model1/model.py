from keras import backend as K
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
from keras import losses
from keras import optimizers
from shared.picture_copy import *
from shared.image_preprocessing import vectorize_images_in_folder
import shared.utility

from shared.image_preprocessing import LoadImagestoArray
from shared.image_Morphological import prepImage
from keras.preprocessing.image import ImageDataGenerator

from keras.applications import VGG16

# Configuration Hierachy
import personal_config
from models.pretrained.pretrained_conf import *
from models.pretrained.model1.configuration import *


def preProcessing_img():
    imgArray = LoadImagestoArray(personal_config.TRAIN_DIR, "png")
    prepImage(PREPROCESS_TRAIN_DIR, imgArray)
    imgArray = LoadImagestoArray(personal_config.TEST_DIR, "png")
    prepImage(PREPROCESS_TEST_DIR, imgArray)
    personal_config.TEST_DIR = PREPROCESS_TEST_DIR
    personal_config.TRAIN_DIR = PREPROCESS_TRAIN_DIR


def prepare_data(save_training_data=None,
                 save_validation_data=None):
    training_directory = personal_config.TRAIN_DIR

    training_data, validation_data = train_val_split_on_folder(source_directory=training_directory,
                                                               train_size=0.8)

    if save_training_data:
        with open(save_training_data, 'w') as td:
            td.write('\n'.join(training_data))
    if save_validation_data:
        with open(save_validation_data, 'w') as tv:
            tv.write('\n'.join(validation_data))

    copy_pictures_arrays(training_data=training_data,
                         validation_data=validation_data,
                         target_train_directory=EXEC_TRAIN_DIR,
                         target_validation_directory=EXEC_VAL_DIR)


# generatePicture
#generatePicture(train_dir=EXEC_TRAIN_DIR, numberof=400)


def train_model(model_save_path,
                class_labels_path,
                evaluation_metrics,
                debug_path,
                epochs,
                early_stop=True,
                best_model=True):


    # MODEL DEFINITION
    if K.image_data_format() == 'channels_first':
        input_shape = (3, IMG_WIDTH, IMG_HEIGHT)
    else:
        input_shape = (IMG_WIDTH, IMG_HEIGHT, 3)

    # Convolutional Base
    conv_base = VGG16(weights='imagenet', include_top= False, input_shape = input_shape)

    # Datagenerator for images
    test_datagen = ImageDataGenerator(rescale=1. / 255)
    train_datagen = ImageDataGenerator(
        rescale=1./255,
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')

    train_generator = train_datagen.flow_from_directory(
                      # This is the target directory
                      EXEC_TRAIN_DIR,
                      # All images will be resized to 150x150
                      target_size=(IMG_HEIGHT, IMG_WIDTH),
                      batch_size=BATCH_SIZE,
                      # Since we use binary_crossentropy loss, we need binary labels
                      class_mode='categorical')

    validation_generator = test_datagen.flow_from_directory(
                           EXEC_VAL_DIR,
                           target_size=(IMG_HEIGHT, IMG_WIDTH),
                           batch_size=BATCH_SIZE,
                           class_mode='categorical')

    nr_train = get_amount_of_files(EXEC_TRAIN_DIR)
    nr_val = get_amount_of_files(EXEC_VAL_DIR)

    class_labels = shared.utility.class_label_generator(EXEC_TRAIN_DIR, class_labels_path)

    model = Sequential()
    model.add(conv_base)
    model.add(Flatten())
    model.add(Dense(256, activation='relu', input_dim=4 * 4 * 512))
    model.add(Dense(12, activation='softmax'))

    print(model.summary())


    conv_base.trainable = False

    model.compile(optimizer=optimizers.RMSprop(lr=2e-5),
                  loss=losses.categorical_crossentropy,
                  metrics=evaluation_metrics)

    callbacks = []
    if early_stop:
        callbacks.append(EarlyStopping(patience=5))
    callbacks.append(ModelCheckpoint(filepath=model_save_path,
                                     save_best_only=best_model,
                                     verbose=1)
                     )
    callbacks.append(TensorBoard(log_dir=debug_path,
                                 histogram_freq=0,
                                 write_graph=True,
                                 write_images=True,
                                 write_grads=True))

    history = model.fit_generator(train_generator,
                                  epochs=epochs,
                                  steps_per_epoch=50,
                                  callbacks=callbacks,
                                  validation_data=validation_generator)

    return history
