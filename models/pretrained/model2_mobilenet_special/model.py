from keras import backend as K
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers.advanced_activations import LeakyReLU
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.models import Sequential, Model, load_model
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard
from keras.callbacks import ReduceLROnPlateau
from keras import losses
from keras import optimizers
from shared.picture_copy import *
from shared.image_preprocessing import vectorize_images_in_folder
import shared.utility

from shared.image_preprocessing import LoadImagestoArray
from shared.image_Morphological import prepImage
from keras.preprocessing.image import ImageDataGenerator

from keras.applications import *

# Configuration Hierachy
import personal_config
from models.pretrained.pretrained_conf import IMG_HEIGHT, IMG_WIDTH, BATCH_SIZE
from models.pretrained.model2_mobilenet.configuration import *

# based on: https://www.kaggle.com/liorbrag/pretrained-xception-on-2gb-gpu-vram-lb-0-95843


def preProcessing_img():
    imgArray = LoadImagestoArray(personal_config.TRAIN_DIR, "png")
    prepImage(PREPROCESS_TRAIN_DIR, imgArray)
    imgArray = LoadImagestoArray(personal_config.TEST_DIR, "png")
    prepImage(PREPROCESS_TEST_DIR, imgArray)
    personal_config.TEST_DIR = PREPROCESS_TEST_DIR
    personal_config.TRAIN_DIR = PREPROCESS_TRAIN_DIR


def prepare_data(save_training_data=None,
                 save_validation_data=None):
    training_directory = personal_config.TRAIN_DIR

    training_data, validation_data = train_val_split_on_folder(source_directory=training_directory,
                                                               train_size=0.8)

    training_data = [item for item in training_data if item.split('/')[0] in ['Black-grass', 'Loose Silky-bent']]
    validation_data = [item for item in validation_data if item.split('/')[0] in ['Black-grass', 'Loose Silky-bent']]
    print(training_data)
    # Balanced Dataset
    no_bg = len([item for item in training_data if item.split('/')[0] in ['Black-grass']])
    no_bg_val = len([item for item in validation_data if item.split('/')[0] in ['Black-grass']])
    print("Length of Black-grass is {0}".format(no_bg))

    counter = 0
    new_training_data = list()
    new_validation_data = list()
    for item in training_data:
        if item.split('/')[0] == 'Loose Silky-bent':
            counter += 1
            if counter > no_bg:
                continue
        new_training_data.append(item)

    counter = 0
    for item in validation_data:
        if item.split('/')[0] == 'Loose Silky-bent':
            counter += 1
            if counter > no_bg_val:
                continue
        new_validation_data.append(item)

    if save_training_data:
        with open(save_training_data, 'w') as td:
            td.write('\n'.join(new_training_data))
    if save_validation_data:
        with open(save_validation_data, 'w') as tv:
            tv.write('\n'.join(new_validation_data))

    copy_pictures_arrays(training_data=new_training_data,
                         validation_data=new_validation_data,
                         target_train_directory=EXEC_TRAIN_DIR,
                         target_validation_directory=EXEC_VAL_DIR)


def train_model(model_save_path,
                class_labels_path,
                evaluation_metrics,
                debug_path,
                epochs,
                early_stop=True,
                best_model=True):

    LEARNING_RATE = 0.001

    if K.image_data_format() == 'channels_first':
        input_shape = (3, IMG_WIDTH, IMG_HEIGHT)
    else:
        input_shape = (IMG_WIDTH, IMG_HEIGHT, 3)

    # Datagenerator for images
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_datagen = ImageDataGenerator(
        rescale=1./255,
        horizontal_flip=True,
        vertical_flip=True,
        width_shift_range=0.2,
        height_shift_range=0.2,
        rotation_range=360,
        zoom_range=0.2)

    train_generator = train_datagen.flow_from_directory(
                      # This is the target directory
                      EXEC_TRAIN_DIR,
                      # All images will be resized to 150x150
                      target_size=(IMG_HEIGHT, IMG_WIDTH),
                      batch_size=BATCH_SIZE,
                      # Since we use binary_crossentropy loss, we need binary labels
                      class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
                           EXEC_VAL_DIR,
                           target_size=(IMG_HEIGHT, IMG_WIDTH),
                           batch_size=BATCH_SIZE,
                           class_mode='binary')

    # Metadata
    class_labels = shared.utility.class_label_generator(EXEC_TRAIN_DIR, class_labels_path)
    nr_train = get_amount_of_files(EXEC_TRAIN_DIR)
    nr_val = get_amount_of_files(EXEC_VAL_DIR)

    K.clear_session()
    # Model Definition
    # Convolutional Base
    conv_base = MobileNet(weights='imagenet', include_top=False, input_shape=input_shape, pooling='avg')

    model = Sequential()
    model.add(conv_base)
    model.add(Dense(256, activation='tanh'))  #try tanh, relu
    model.add(LeakyReLU(alpha=.001))  # add an advanced activation
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer=optimizers.Adam(lr=LEARNING_RATE),
                  loss=losses.binary_crossentropy,
                  metrics=evaluation_metrics)

    callbacks = []
    if early_stop:
        callbacks.append(EarlyStopping(monitor='val_loss',
                                       patience=20,
                                       verbose=1))

    callbacks.append(ModelCheckpoint(filepath=model_save_path,
                                     save_best_only=best_model,
                                     monitor='val_loss',
                                     verbose=1))

    # Reduce Learning Rate on Plateau
    callbacks.append(ReduceLROnPlateau(monitor='val_loss',
                                       factor=0.1,
                                       patience=2,
                                       verbose=1,
                                       mode='auto',
                                       epsilon=0.0001,
                                       cooldown=0,
                                       min_lr=0))

    callbacks.append(TensorBoard(log_dir=debug_path,
                                 histogram_freq=0,
                                 write_graph=True,
                                 write_images=True,
                                 write_grads=True))

    history = model.fit_generator(train_generator,
                                  epochs=epochs,
                                  steps_per_epoch=nr_train/(BATCH_SIZE/2),
                                  callbacks=callbacks,
                                  validation_data=validation_generator,
                                  validation_steps=nr_val/(BATCH_SIZE/2),
                                  verbose=1)

    return history
