import os
from personal_config import DATA

PREPROCESS_TRAIN_DIR = os.path.join(DATA, '{0}_preprocess_train'.format(os.path.basename(os.path.dirname(__file__))))
PREPROCESS_TEST_DIR = os.path.join(DATA, '{0}_preprocess_test'.format(os.path.basename(os.path.dirname(__file__))))
EXEC_TRAIN_DIR = os.path.join(DATA, '{0}_train'.format(os.path.basename(os.path.dirname(__file__))))
EXEC_VAL_DIR = os.path.join(DATA, '{0}_val'.format(os.path.basename(os.path.dirname(__file__))))
